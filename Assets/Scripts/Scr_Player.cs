﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Scr_Controller2D))] //Scr_Player need "Scr_Controller2D" for works. 
public class Scr_Player : MonoBehaviour {

    Vector3 initialPosition;

    public float jumpHeight = 4;
    public float timeToJumpApex = .4f;
    float accelerationTimeAirborne = .2f;
    float accelerationTimeGrounded = .1f;
    float moveSpeed = 6;

    private Animator anim;

    private bool isJumping = false;
    private bool isRunning = false;


    public Vector2 wallJumpClimb;
    public Vector2 wallJumpOff;
    public Vector2 wallLeap;

    public float wallSlideSpeedMax = 3;

    float gravity;
    float jumpVelocity;
    Vector2 velocity;
    float targetVelocityX;
    float velocityXSmoothing;

    bool wallSliding;


    Scr_Controller2D controller;

    // Use this for initialization
    void Start() {
        initialPosition = GetComponent<Transform>().localPosition;

        controller = GetComponent<Scr_Controller2D>();

        gravity = -(2 * jumpHeight) / Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update() {
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        int wallDirX = (controller.collisions.left) ? -1 : 1;

        wallSliding = false;

        if ((controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
        {
            wallSliding = true;

            if (velocity.y < -wallSlideSpeedMax)
            {
                velocity.y = -wallSlideSpeedMax;
            }
        }

        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
            isJumping = false;
        }


        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = true;
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (wallSliding)
            {
                if (wallDirX == input.x)
                {
                    velocity.x = -wallDirX * wallJumpClimb.x;
                    velocity.y = wallJumpClimb.y;
                } else if (input.x == 0)
                {
                    velocity.x = -wallDirX * wallJumpOff.x;
                    velocity.y = wallJumpOff.y;
                } else
                {
                    velocity.x = -wallDirX * wallLeap.x;
                    velocity.y = wallLeap.y;
                }
            }
            if (controller.collisions.below)
            {
                velocity.y = jumpVelocity;

            }

        }

        targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below) ? accelerationTimeGrounded : accelerationTimeAirborne);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);

        if (velocity.x >= 0.5)
        {
            isRunning = true;
            GetComponent<SpriteRenderer>().flipX = false;
        }else if (velocity.x <= -0.5)
        {
            isRunning = true;
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            isRunning = false;
        }


        if (isJumping) {
            anim.SetBool("jumping", true);
        } else{
            anim.SetBool("jumping", false);
        }

        if (isRunning)
        {
            anim.SetBool("running", true);
        }
        else
        {
            anim.SetBool("running", false);
        }

        

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.tag == "dead")
        {
            Debug.Log("Muere cabron!!!");
            gameObject.GetComponent<Transform>().localPosition = initialPosition;
        }
    }

}
